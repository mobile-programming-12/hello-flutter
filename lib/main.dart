import 'package:flutter/material.dart';

void main() {
  runApp(HelloFlutterApp());
}

class HelloFlutterApp extends StatefulWidget{
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String ukraineGreeting = "привіт флаттер";
String japaneseGreeting = "こんにちはフラッター";


class _MyStatefulWidgetState extends State<HelloFlutterApp> {
  String displayText = englishGreeting;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("Hello Flutter"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(
              onPressed: () {
                setState(() {
                  displayText = displayText == englishGreeting? 
                  spanishGreeting : englishGreeting;
                });
              }, icon: Icon(Icons.refresh)),
              IconButton(
                onPressed: (){
                  setState(() {
                    displayText = displayText == ukraineGreeting? 
                  englishGreeting : ukraineGreeting;
                  });
                }, icon: Icon(Icons.abc)),
                IconButton(
                onPressed: (){
                  setState(() {
                    displayText = displayText == japaneseGreeting? 
                  englishGreeting : japaneseGreeting;
                  });
                }, icon: Icon(Icons.language))
          ],
        ),
        body: Center(
            child: Text(
          displayText,
          style: TextStyle(fontSize: 24),
        )),
      ),
    );
  }
}

// class HelloFlutterApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Hello Flutter"),
//           leading: Icon(Icons.home),
//           actions: <Widget>[
//             IconButton(onPressed: () {}, icon: Icon(Icons.refresh))
//           ],
//         ),
//         body: Center(
//             child: Text(
//           "Hello flutter !",
//           style: TextStyle(fontSize: 24),
//         )),
//       ),
//     );
//   }
// }
